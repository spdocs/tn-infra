####### VPC Configuration

module "vpc" {
  source = "bitbucket.org/projectwaffle/module-aws-vpc.git?ref=tags/v0.0.1"

  name = "${var.name}"

  resource_identities = {
    vpc                 = "vpc"
    dhcp_options        = "dhcpoptions"
    internet_gateway    = "igw"
    route_table_public  = "public"
    route_table_private = "private"
    route_table_dmz     = "dmz"
    route_table_intra   = "intra"
    subnet              = "subnet"
    eip_nat             = "nat"
    nat_gateway         = "nat"
  }

  global_tags = "${var.global_tags}"

  public_subnet_tags = {
    role                     = "public"
    "kubernetes.io/role/elb" = "1"
  }

  enable_nat_gateway     = "false"
  one_nat_gateway_per_az = "false"

  cidr_block           = "10.93.0.0/16"
  enable_dns_hostnames = "${var.private_dns_zone_name == "" && var.public_dns_zone_name == ""}"

  #  enable_dhcp_options      = "${var.private_dns_zone_name == "" && var.public_dns_zone_name == ""}"
  #  dhcp_options_domain_name = "${var.private_dns_zone_name == "" && var.public_dns_zone_name == "" ? "cluster.net" : ""}"

  azs             = ["eu-west-1a", "eu-west-1b", "eu-west-1c"]
  public_subnets  = ["10.93.0.0/23", "10.93.2.0/23", "10.93.4.0/23"]
  create_dmz      = "true"
  dmz_subnets     = ["10.93.10.0/23", "10.93.12.0/23", "10.93.14.0/23"]
  private_subnets = ["10.93.20.0/23", "10.93.22.0/23", "10.93.24.0/23"]
  create_intra    = "true"
  intra_subnets   = ["10.93.30.0/23", "10.93.32.0/23", "10.93.34.0/23"]
}

####### Kubeadm Token modules creation

module "kubeadm-token" {
  source = "bitbucket.org/projectwaffle/module-kubeadm-token.git?ref=tags/v0.0.1"
}

####### IAM ROles

module "master_role" {
  source = "bitbucket.org/projectwaffle/module-aws-iam.git?ref=tags/v0.0.1"

  resource_identities = {
    assume_role = "master"
  }

  name               = "${var.name}"
  create_assume_role = "${var.master_mode != ""}"
  global_tags        = "${var.global_tags}"

  assume_role_assume_policy = [
    {
      statement_efect                  = "Allow"
      statement_principals_type        = "Service"
      statement_principals_identifiers = "ec2.amazonaws.com"
    },
  ]

  assume_role_policy = [
    {
      statement_efect     = "Allow"
      statement_actions   = "ssm:*,s3:*,acm:*,ec2:*,elasticloadbalancing:*,ecr:GetAuthorizationToken,ecr:BatchCheckLayerAvailability,ecr:GetDownloadUrlForLayer,ecr:GetRepositoryPolicy,ecr:DescribeRepositories,ecr:ListImages,ecr:BatchGetImage,route53:GetHostedZone,route53:ListHostedZones,route53:ListHostedZonesByName,route53:ChangeResourceRecordSets,route53:ListResourceRecordSets,route53:GetChange,autoscaling:DescribeAutoScalingGroups,autoscaling:DescribeAutoScalingInstances,autoscaling:DescribeTags,autoscaling:SetDesiredCapacity,autoscaling:TerminateInstanceInAutoScalingGroup,autoscaling:DescribeLaunchConfigurations"
      statement_resources = "*"
    },
    {
      statement_efect     = "Allow"
      statement_actions   = "ssm:DescribeAssociation,ssm:GetDeployablePatchSnapshotForInstance,ssm:GetDocument,ssm:GetManifest,ssm:GetParameters,ssm:ListAssociations,ssm:ListInstanceAssociations,ssm:PutInventory,ssm:PutComplianceItems,ssm:PutConfigurePackageResult,ssm:UpdateAssociationStatus,ssm:UpdateInstanceAssociationStatus,ssm:UpdateInstanceInformation,ssmmessages:CreateControlChannel,ssmmessages:CreateDataChannel,ssmmessages:OpenControlChannel,ssmmessages:OpenDataChannel,ec2messages:AcknowledgeMessage,ec2messages:DeleteMessage,ec2messages:FailMessage,ec2messages:GetEndpoint,ec2messages:GetMessages,ec2messages:SendReply,cloudwatch:PutMetricData,ec2:DescribeInstanceStatus,ds:CreateComputer,ds:DescribeDirectories,logs:CreateLogGroup,logs:CreateLogStream,logs:DescribeLogGroups,logs:DescribeLogStreams,logs:PutLogEvents"
      statement_resources = "*"
    },
  ]
}

module "node_role" {
  source = "bitbucket.org/projectwaffle/module-aws-iam.git?ref=tags/v0.0.1"

  resource_identities = {
    assume_role = "node"
  }

  name               = "${var.name}"
  create_assume_role = "${var.master_mode != ""}"
  global_tags        = "${var.global_tags}"

  assume_role_assume_policy = [
    {
      statement_efect                  = "Allow"
      statement_principals_type        = "Service"
      statement_principals_identifiers = "ec2.amazonaws.com"
    },
  ]

  assume_role_policy = [
    {
      statement_efect     = "Allow"
      statement_actions   = "vpc:*,ec2:*,waf-regional:*,elasticloadbalancing:*,tag:*,ec2:Describe*,ecr:GetAuthorizationToken,ecr:BatchCheckLayerAvailability,ecr:GetDownloadUrlForLayer,ecr:GetRepositoryPolicy,ecr:DescribeRepositories,ecr:ListImages,ecr:BatchGetImage"
      statement_resources = "*"
    },
    {
      statement_efect     = "Allow"
      statement_actions   = "ssm:DescribeAssociation,ssm:GetDeployablePatchSnapshotForInstance,ssm:GetDocument,ssm:GetManifest,ssm:GetParameters,ssm:ListAssociations,ssm:ListInstanceAssociations,ssm:PutInventory,ssm:PutComplianceItems,ssm:PutConfigurePackageResult,ssm:UpdateAssociationStatus,ssm:UpdateInstanceAssociationStatus,ssm:UpdateInstanceInformation,ssmmessages:CreateControlChannel,ssmmessages:CreateDataChannel,ssmmessages:OpenControlChannel,ssmmessages:OpenDataChannel,ec2messages:AcknowledgeMessage,ec2messages:DeleteMessage,ec2messages:FailMessage,ec2messages:GetEndpoint,ec2messages:GetMessages,ec2messages:SendReply,cloudwatch:PutMetricData,ec2:DescribeInstanceStatus,ds:CreateComputer,ds:DescribeDirectories,logs:CreateLogGroup,logs:CreateLogStream,logs:DescribeLogGroups,logs:DescribeLogStreams,logs:PutLogEvents"
      statement_resources = "*"
    },
  ]
}

module "node_spot_fleet_role" {
  source = "bitbucket.org/projectwaffle/module-aws-iam.git?ref=tags/v0.0.1"

  resource_identities = {
    assume_role = "node-spot-fleet"
  }

  name               = "${var.name}"
  create_assume_role = "${var.node_spot_count > 0 || var.master_mode == "ha"}"
  global_tags        = "${var.global_tags}"

  assume_role_assume_policy = [
    {
      statement_efect                  = "Allow"
      statement_principals_type        = "Service"
      statement_principals_identifiers = "spotfleet.amazonaws.com"
    },
  ]

  assume_role_policy = [
    {
      statement_efect     = "Allow"
      statement_actions   = "ec2:RunInstances,ec2:DescribeImages,ec2:DescribeSubnets,ec2:RequestSpotInstances,ec2:TerminateInstances,ec2:DescribeInstanceStatus,am:PassRole,elasticloadbalancing:RegisterTargets"
      statement_resources = "*"
    },
    {
      statement_efect     = "Allow"
      statement_actions   = "elasticloadbalancing:RegisterInstancesWithLoadBalancer"
      statement_resources = "arn:aws:elasticloadbalancing:*:*:loadbalancer/*"
    },
    {
      statement_efect     = "Allow"
      statement_actions   = "ec2:CreateTags"
      statement_resources = "arn:aws:ec2:*:*:instance/*,arn:aws:ec2:*:*:spot-instances-request/*"
    },
  ]

  assume_role_policy_condition = [
    {
      statement_efect     = "Allow"
      statement_actions   = "iam:PassRole"
      statement_resources = "*"
      condition_test      = "StringEquals"
      condition_variable  = "am:PassedToService"
      condition_values    = "ec2.amazonaws.com,ec2.amazonaws.com.cn"
    },
  ]
}

module "build_role" {
  source = "bitbucket.org/projectwaffle/module-aws-iam.git?ref=tags/v0.0.1"

  resource_identities = {
    assume_role = "build"
  }

  name               = "${var.name}"
  create_assume_role = "true"
  global_tags        = "${var.global_tags}"

  assume_role_assume_policy = [
    {
      statement_efect                  = "Allow"
      statement_principals_type        = "Service"
      statement_principals_identifiers = "codebuild.amazonaws.com"
    },
  ]

  assume_role_policy = [
    {
      statement_efect     = "Allow"
      statement_actions   = "ecr:*,logs:CreateLogGroup,logs:CreateLogStream,logs:PutLogEvents,ec2:CreateNetworkInterface,ec2:DescribeNetworkInterfaces,ec2:DeleteNetworkInterface,ec2:DescribeSubnets,ec2:DescribeSecurityGroups,ec2:DescribeVpcs,s3:*"
      statement_resources = "*"
    }
  ]
}


########## Security Groups

module "sg_master" {
  source = "bitbucket.org/projectwaffle/module-aws-security-group.git?ref=tags/v0.0.1"

  resource_identities = {
    security_group = "sg-master"
  }

  name = "${var.name}"

  global_tags           = "${var.global_tags}"
  create_security_group = "${var.master_mode != ""}"
  vpc_id                = "${module.vpc.vpc_id}"

  ingress_with_cidr_blocks = [
    {
      from_port   = "6443"
      to_port     = "6443"
      protocol    = "TCP"
      description = "Kubernetes API server"
      cidr_blocks = "0.0.0.0/0"
    },
    {
      from_port   = "22"
      to_port     = "22"
      protocol    = "TCP"
      description = "Kubernetes API server"
      cidr_blocks = "0.0.0.0/0"
    },
    {
      from_port   = "2379"
      to_port     = "2380"
      protocol    = "TCP"
      description = "etcd server client API"
      cidr_blocks = "${module.vpc.vpc_cidr_block}"
    },
    {
      from_port   = "10250"
      to_port     = "10250"
      protocol    = "TCP"
      description = "Kubelet API"
      cidr_blocks = "${module.vpc.vpc_cidr_block}"
    },
    {
      from_port   = "10251"
      to_port     = "10251"
      protocol    = "TCP"
      description = "kube-scheduler"
      cidr_blocks = "${module.vpc.vpc_cidr_block}"
    },
    {
      from_port   = "10252"
      to_port     = "10252"
      protocol    = "TCP"
      description = "kube-controller-manager"
      cidr_blocks = "${module.vpc.vpc_cidr_block}"
    },
    {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      description = "Intra cluster Services"
      cidr_blocks = "${module.vpc.vpc_cidr_block}"
    },
  ]

  egress_with_cidr_blocks = [
    {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      description = "Allow Any"
      cidr_blocks = "0.0.0.0/0"
    },
  ]
}

module "sg_node" {
  source = "bitbucket.org/projectwaffle/module-aws-security-group.git?ref=tags/v0.0.1"

  resource_identities = {
    security_group = "sg-node"
  }

  name = "${var.name}"

  global_tags           = "${var.global_tags}"
  create_security_group = "${var.master_mode != ""}"
  vpc_id                = "${module.vpc.vpc_id}"

  ingress_with_cidr_blocks = [
    {
      from_port   = "10250"
      to_port     = "10250"
      protocol    = "TCP"
      description = "Kubelet API"
      cidr_blocks = "${module.vpc.vpc_cidr_block}"
    },
    {
      from_port   = "30000"
      to_port     = "32767"
      protocol    = "TCP"
      description = "NodePort Services"
      cidr_blocks = "${module.vpc.vpc_cidr_block}"
    },
    {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      description = "Intra cluster Services"
      cidr_blocks = "${module.vpc.vpc_cidr_block}"
    },
    {
      from_port   = "22"
      to_port     = "22"
      protocol    = "TCP"
      description = "Kubernetes SSH server"
      cidr_blocks = "0.0.0.0/0"
    },
  ]

  egress_with_cidr_blocks = [
    {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      description = "Allow Any"
      cidr_blocks = "0.0.0.0/0"
    },
  ]
}

############ HA Configuration 

module "tg_master_lb" {
  source = "bitbucket.org/projectwaffle/module-aws-lb-target-group.git?ref=tags/v0.0.1"

  name = "${var.name}"

  resource_identities = {
    tg = "api"
  }

  global_tags           = "${var.global_tags}"
  create_target_group   = "${var.master_mode == "ha"}"
  vpc_id                = "${module.vpc.vpc_id}"
  port                  = "6443"
  protocol              = "TCP"
  health_check_protocol = "TCP"
  target_type           = "${var.master_mode != "" && var.private_dns_zone_name == "" && var.public_dns_zone_name == ""? "ip" : "instance"}"
  load_balancer_type    = "network"
}

module "lb_master" {
  source = "bitbucket.org/projectwaffle/module-aws-lb.git?ref=tags/v0.0.1"

  name = "${var.name}"

  resource_identities = {
    lb = "api-lb"
  }

  global_tags = "${var.global_tags}"

  create_lb                = "${var.master_mode == "ha"}"
  internal                 = "${var.public_dns_zone_name == ""}"
  subnets                  = "${module.vpc.public_subnets}"
  load_balancer_type       = "network"
  default_target_group_arn = "${module.tg_master_lb.default_target_group_arn}"

  forward_listener = [
    {
      port     = "6443"
      protocol = "TCP"
    },
  ]
}

locals {
  cluster_ha_lb_dns_name = "${element(concat(list(module.lb_master.lb_dns_name), list("")), 0)}"
}

############ Bootstraping scrips

data "template_file" "init_master" {
  count = "${var.master_mode != "" ? 1 : 0}"

  template = "${file("scripts/init-aws-kubernetes-master-${var.master_mode}.sh")}"

  vars {
    kubeadm_token          = "${module.kubeadm-token.token}"
    aws_region             = "eu-west-1"
    dns_name               = "${var.kubernetes_cluster_name}.${local.dns_zone_name}"
    cluster_name           = "${var.kubernetes_cluster_name}"
    kubernetes_version     = "${var.kubernetes_cluster_version}"
    cluster_dns_domain     = "${var.kubernetes_cluster_locl_dns_name}"
    cluster_pod_subnet     = "${var.kubernetes_cluster_pod_subnet}"
    cluster_ha_lb_dns_name = "${local.cluster_ha_lb_dns_name}"
    cluster_ha_rest        = "${var.master_mode == "ha" ? join(",", module.master_ha_rest.id) : "" }"
    addons                 = "${join(" ", var.addons)}"
  }
}

data "template_cloudinit_config" "master_cloud_init" {
  count = "${var.master_mode != "" ? 1 : 0}"

  gzip          = false
  base64_encode = false

  part {
    filename     = "init-aws-kubernete-master.sh"
    content_type = "text/x-shellscript"
    content      = "${element(concat(data.template_file.init_master.*.rendered, list("")), 0)}"
  }
}

data "template_file" "init_master_rest" {
  count = "${var.master_mode == "ha" ? 1 : 0}"

  template = "${file("scripts/init-aws-kubernetes-master-spot.sh")}"

  vars {
    kubeadm_token          = "${module.kubeadm-token.token}"
    dns_name               = "${var.kubernetes_cluster_name}.${local.dns_zone_name}"
    cluster_name           = "${var.kubernetes_cluster_name}"
    kubernetes_version     = "${var.kubernetes_cluster_version}"
    labels                 = "instance=ondemand,node-role.kubernetes.io/master="
    cluster_ha_lb_dns_name = "${local.cluster_ha_lb_dns_name}"
  }
}

data "template_cloudinit_config" "master_rest_cloud_init" {
  count = "${var.master_mode == "ha" ? 1 : 0}"

  gzip          = false
  base64_encode = false

  part {
    filename     = "init-aws-kubernete-master-spot.sh"
    content_type = "text/x-shellscript"
    content      = "${element(concat(data.template_file.init_master_rest.*.rendered, list("")), 0)}"
  }
}

data "template_file" "init_node_ondemand" {
  count = "${var.node_ondemand_count > 0 ? 1 : 0}"

  template = "${file("scripts/init-aws-kubernetes-node.sh")}"

  vars {
    kubeadm_token      = "${module.kubeadm-token.token}"
    dns_name           = "${var.kubernetes_cluster_name}.${local.dns_zone_name}"
    cluster_name       = "${var.kubernetes_cluster_name}"
    kubernetes_version = "${var.kubernetes_cluster_version}"
    labels             = "instance=ondemand,node-role.kubernetes.io/node="
  }
}

data "template_cloudinit_config" "node_ondemand_cloud_init" {
  count = "${var.node_ondemand_count > 0 ? 1 : 0}"

  gzip          = false
  base64_encode = false

  part {
    filename     = "init-aws-kubernete-node.sh"
    content_type = "text/x-shellscript"
    content      = "${element(concat(data.template_file.init_node_ondemand.*.rendered, list("")), 0)}"
  }
}

data "template_file" "init_node_spot" {
  count = "${var.node_spot_count > 0 ? 1 : 0}"

  template = "${file("scripts/init-aws-kubernetes-node.sh")}"

  vars {
    kubeadm_token      = "${module.kubeadm-token.token}"
    dns_name           = "${var.kubernetes_cluster_name}.${local.dns_zone_name}"
    cluster_name       = "${var.kubernetes_cluster_name}"
    kubernetes_version = "${var.kubernetes_cluster_version}"
    labels             = "instance=spot,node-role.kubernetes.io/node="
  }
}

data "template_cloudinit_config" "node_spot_cloud_init" {
  count = "${var.node_spot_count > 0 ? 1 : 0}"

  gzip          = false
  base64_encode = false

  part {
    filename     = "init-aws-kubernete-node.sh"
    content_type = "text/x-shellscript"
    content      = "${element(concat(data.template_file.init_node_spot.*.rendered, list("")), 0)}"
  }
}

################  Compute Instances

module "master" {
  source = "bitbucket.org/projectwaffle/module-aws-ec2-instance.git?ref=tags/v0.0.1"

  name = "${var.name}"

  resource_identities = {
    key_name = "keypair"
    eip      = "eip"
  }

  global_tags = "${var.global_tags}"

  module_tags = {
    KubernetesCluster = "${var.kubernetes_cluster_name}"
  }

  create_eip                        = "true"
  create_instance                   = "${var.master_mode == "standalone"}"
  instance_type                     = "${var.master_instance_type}"
  instance_count                    = "1"
  root_block_device_ebs_volume_size = "40"
  vpc_security_group_ids            = ["${module.sg_master.security_group_id}"]
  subnet_id                         = "${module.vpc.private_subnets}"
  subnet_az                         = "${module.vpc.private_subnets_azs}"
  instance_name_prefix              = "${var.name}-master"
  key_name                          = "${var.keypair_name}"
  iam_instance_profile              = "${module.master_role.assume_role_instance_profile_name}"
  create_data_block_device_ebs      = "false"
  data_block_device_ebs_count       = "1"
  create_custom_userdata            = "${var.master_mode == "standalone"}"
  custom_userdata_script_content    = "${element(concat(data.template_cloudinit_config.master_cloud_init.*.rendered, list("")), 0)}"
}

module "master_ha_first" {
  source = "bitbucket.org/projectwaffle/module-aws-ec2-instance.git?ref=tags/v0.0.2"

  name = "${var.name}"

  resource_identities = {
    key_name = "keypair"
    eip      = "eip"
  }

  global_tags = "${var.global_tags}"

  module_tags = {
    KubernetesCluster = "${var.kubernetes_cluster_name}"
  }

  create_instance                    = "${var.master_mode == "ha"}"
  instance_type                      = "${var.master_instance_type}"
  instance_count                     = "1"
  root_block_device_ebs_volume_size  = "40"
  vpc_security_group_ids             = ["${module.sg_master.security_group_id}"]
  subnet_id                          = "${list(element(module.vpc.private_subnets, 0))}"
  subnet_az                          = "${list(element(module.vpc.private_subnets_azs, 0))}"
  instance_name_prefix               = "${var.name}-master-first"
  key_name                           = "${var.keypair_name}"
  iam_instance_profile               = "${module.master_role.assume_role_instance_profile_name}"
  key_name                           = "${var.keypair_name}"
  create_custom_userdata             = "${var.master_mode == "ha"}"
  custom_userdata_script_content     = "${element(concat(data.template_cloudinit_config.master_cloud_init.*.rendered, list("")), 0)}"
  associate_target_group_arns        = "true"
  associate_target_group_instance_ip = "${var.master_mode != "" && var.private_dns_zone_name == "" && var.public_dns_zone_name == ""}"
  target_group_arns                  = ["${module.tg_master_lb.default_target_group_arn}"]
}

module "master_ha_rest" {
  source = "bitbucket.org/projectwaffle/module-aws-ec2-instance.git?ref=tags/v0.0.2"

  name = "${var.name}"

  resource_identities = {
    key_name = "keypair"
    eip      = "eip"
  }

  global_tags = "${var.global_tags}"

  module_tags = {
    KubernetesCluster = "${var.kubernetes_cluster_name}"
  }

  create_instance                    = "${var.master_mode == "ha"}"
  instance_type                      = "${var.master_instance_type}"
  instance_count                     = "2"
  root_block_device_ebs_volume_size  = "40"
  vpc_security_group_ids             = ["${module.sg_master.security_group_id}"]
  subnet_id                          = "${list(element(module.vpc.private_subnets, 1), element(module.vpc.private_subnets, 2))}"
  subnet_az                          = "${list(element(module.vpc.private_subnets_azs, 1), element(module.vpc.private_subnets_azs, 2))}"
  instance_name_prefix               = "${var.name}-master-rest"
  key_name                           = "${var.keypair_name}"
  iam_instance_profile               = "${module.master_role.assume_role_instance_profile_name}"
  key_name                           = "${var.keypair_name}"
  create_custom_userdata             = "${var.master_mode == "ha"}"
  custom_userdata_script_content     = "${element(concat(data.template_cloudinit_config.master_rest_cloud_init.*.rendered, list("")), 0)}"
  associate_target_group_arns        = "true"
  associate_target_group_instance_ip = "${var.master_mode != "" && var.private_dns_zone_name == "" && var.public_dns_zone_name == ""}"
  target_group_arns                  = ["${module.tg_master_lb.default_target_group_arn}"]
}

data "aws_route53_zone" "public" {
  count = "${var.master_mode != "" && var.public_dns_zone_name != "" ? 1 : 0}"

  name = "${var.public_dns_zone_name}"
}

data "aws_route53_zone" "private" {
  count = "${var.master_mode != "" && var.private_dns_zone_name != "" ? 1 : 0}"

  name = "${var.private_dns_zone_name}"
}

resource "aws_route53_zone" "local" {
  count = "${var.master_mode != "" && var.private_dns_zone_name == "" && var.public_dns_zone_name == ""? 1 : 0}"

  name    = "cluster.net"
  comment = "VPC internal DNS zone"

  vpc {
    vpc_id     = "${module.vpc.vpc_id}"
    vpc_region = "eu-west-1"
  }
}

locals {
  cluster_ha_dns_name                = "${element(concat(list(module.lb_master.lb_dns_name), list("")), 0)}"
  cluster_standalone_public_dns_name = "${element(module.master.eip_public_ip, 0) != "" ? element(module.master.eip_public_ip, 0) : join("", module.master.public_ip)}"
  cluster_standalone_dns_name        = "${var.public_dns_zone_name != "" ? local.cluster_standalone_public_dns_name : join("", module.master.private_ip)}"
  dns_zone_id                        = "${element(concat(data.aws_route53_zone.public.*.zone_id, data.aws_route53_zone.private.*.zone_id, aws_route53_zone.local.*.zone_id, list("")), 0)}"
  dns_zone_name                      = "${var.public_dns_zone_name != "" ? var.public_dns_zone_name : (var.private_dns_zone_name != "" ? var.private_dns_zone_name : "cluster.net") }"
}

resource "aws_route53_record" "main" {
  count = "${var.master_mode != "" && var.master_mode == "standalone" ? 1 : 0}"

  zone_id = "${local.dns_zone_id}"
  name    = "${var.kubernetes_cluster_name}.${local.dns_zone_name}"
  type    = "A"
  ttl     = "300"
  records = ["${local.cluster_standalone_dns_name}"]
}

resource "aws_route53_record" "alias" {
  count = "${var.master_mode != "" && var.master_mode == "ha" ? 1 : 0}"

  zone_id = "${local.dns_zone_id}"
  name    = "${var.kubernetes_cluster_name}.${local.dns_zone_name}"
  type    = "A"

  alias {
    name                   = "${module.lb_master.lb_dns_name}"
    zone_id                = "${module.lb_master.lb_zone_id}"
    evaluate_target_health = "false"
  }
}

## Nodes 

module "node_ondemand" {
  source = "bitbucket.org/projectwaffle/module-aws-ec2-instance.git?ref=tags/v0.0.1"

  name = "${var.name}"

  resource_identities = {
    key_name = "keypair"
    eip      = "eip"
  }

  global_tags = "${var.global_tags}"

  module_tags = {
    KubernetesCluster = "${var.kubernetes_cluster_name}"
  }

  create_instance                   = "${var.node_ondemand_count > 0}"
  instance_type                     = "${var.node_ondemand_instance_type}"
  instance_count                    = "${var.node_ondemand_count}"
  root_block_device_ebs_volume_size = "60"
  vpc_security_group_ids            = ["${module.sg_node.security_group_id}"]
  subnet_id                         = "${module.vpc.private_subnets}"
  subnet_az                         = "${module.vpc.private_subnets_azs}"
  instance_name_prefix              = "${var.name}-node-od"
  key_name                          = "${var.keypair_name}"
  iam_instance_profile              = "${module.node_role.assume_role_instance_profile_name}"
  create_data_block_device_ebs      = "false"
  data_block_device_ebs_count       = "1"
  create_custom_userdata            = "${var.node_ondemand_count > 0}"
  custom_userdata_script_content    = "${element(concat(data.template_cloudinit_config.node_ondemand_cloud_init.*.rendered, list("")), 0)}"
}

module "node_spot" {
  source = "bitbucket.org/projectwaffle/module-aws-spot-instance.git?ref=tags/v0.0.1"

  name = "${var.name}"

  resource_identities = {
    key_name = "keypair"
  }

  global_tags = "${var.global_tags}"

  module_tags = {
    KubernetesCluster = "${var.kubernetes_cluster_name}"
  }

  valid_from                        = "2018-12-03T11:39:57Z"
  valid_until                       = "2019-12-04T11:39:57Z"
  create_spot                       = "${var.node_spot_count > 0}"
  target_capacity                   = "${var.node_spot_count}"
  instance_type                     = "${var.node_spot_instance_type}"
  root_block_device_ebs_volume_size = "40"
  vpc_security_group_ids            = ["${module.sg_node.security_group_id}"]
  subnet_id                         = "${module.vpc.private_subnets}"
  subnet_az                         = "${module.vpc.private_subnets_azs}"
  iam_fleet_role                    = "${module.node_spot_fleet_role.assume_role_arn}"
  iam_instance_profile              = "${module.node_role.assume_role_instance_profile_name}"
  instance_name_prefix              = "${var.name}-node-spot"
  key_name                          = "${var.keypair_name}"
  create_custom_userdata            = "${var.node_spot_count > 0}"
  custom_userdata_script_content    = "${element(concat(data.template_cloudinit_config.node_spot_cloud_init.*.rendered, list("")), 0)}"
}

## ECR Configuration

resource "aws_ecr_repository" "main" {
  name = "${var.name}-api"
}

## Build Configuration

resource "aws_codebuild_project" "main" {
  name          = "${var.name}-build"
  build_timeout = "5"
  service_role  = "${module.build_role.assume_role_arn}"


  environment {
    compute_type = "BUILD_GENERAL1_SMALL"
    image        = "aws/codebuild/docker:18.09.0"
    type         = "LINUX_CONTAINER"
    privileged_mode = "true"
  }

  artifacts {
    type = "NO_ARTIFACTS"
  }

  source = [
    {
      type                = "BITBUCKET"
      location            = "https://sergiuplotnicu@bitbucket.org/spdocs/tn-api.git"
      report_build_status = "true"

      buildspec = <<-EOF
      version: 0.2

      phases:
        pre_build:
          commands:
             - $(aws ecr get-login --region $AWS_DEFAULT_REGION --no-include-email)
        build:
          commands:			 
             - docker build -t ${aws_ecr_repository.main.repository_url}:latest .
             - docker push ${aws_ecr_repository.main.repository_url}:latest
      EOF
    },
  ]
}

## RDS Instance Configuration

resource "aws_db_subnet_group" "main" {
  name       = "${var.name}-db-subnet-group"
  subnet_ids = ["${module.vpc.private_subnets}"]

  tags = "${var.global_tags}"
}

module "sg_db" {
  source = "bitbucket.org/projectwaffle/module-aws-security-group.git?ref=tags/v0.0.1"

  resource_identities = {
    security_group = "sg-db"
  }

  name = "${var.name}"

  global_tags           = "${var.global_tags}"
  create_security_group = "${var.master_mode != ""}"
  vpc_id                = "${module.vpc.vpc_id}"

  ingress_with_cidr_blocks = [
    {
      from_port   = "3306"
      to_port     = "3306"
      protocol    = "TCP"
      description = "MySQL"
      cidr_blocks = "${module.vpc.vpc_cidr_block}"
    }
  ]

  egress_with_cidr_blocks = [
    {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      description = "Allow Any"
      cidr_blocks = "0.0.0.0/0"
    },
  ]
}

resource "aws_db_instance" "main" {
  allocated_storage    = 5
  storage_type         = "gp2"
  engine               = "mysql"
  engine_version       = "5.7"
  instance_class       = "db.t2.micro"
  db_subnet_group_name = "${aws_db_subnet_group.main.name}"
  vpc_security_group_ids = ["${module.sg_db.security_group_id}"]
  skip_final_snapshot  = "true"
  identifier           = "${var.db_name}"
  name                 = "${var.db_name}"
  username             = "${var.db_username}"
  password             = "${var.db_password}"
}
