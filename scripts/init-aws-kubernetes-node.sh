#!/bin/bash

set -o verbose
set -o errexit
set -o pipefail

export KUBEADM_TOKEN=${kubeadm_token}
export DNS_NAME=${dns_name}
export KUBERNETES_VERSION="${kubernetes_version}"
export CLUSTER_NAME=${cluster_name}

# Set this only after setting the defaults
set -o nounset

# We needed to match the hostname expected by kubeadm an the hostname used by kubelet
FULL_HOSTNAME="$(curl -s http://169.254.169.254/latest/meta-data/hostname)"
IP_ADDRESS="$(curl -s http://169.254.169.254/latest/meta-data/local-ipv4)"

#Set Hostname for Linux Host
sudo hostnamectl set-hostname $FULL_HOSTNAME
sudo echo "HOSTNAME=$FULL_HOSTNAME" >> /etc/sysconfig/network
sudo echo "$IP_ADDRESS $FULL_HOSTNAME" >> /etc/hosts
sudo /etc/init.d/network restart


# Make DNS lowercase
DNS_NAME=$(echo "$DNS_NAME" | tr 'A-Z' 'a-z')


# Install docker
sudo amazon-linux-extras install epel docker -y
sudo yum update -y
# Install Kubernetes components
sudo cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=0
repo_gpgcheck=0
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg
        https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF

# setenforce returns non zero if already SE Linux is already disabled
is_enforced=$(getenforce)
if [[ $is_enforced != "Disabled" ]]; then
  setenforce 0
fi

yum install -y ipvsadm tc
yum install -y kubelet-$KUBERNETES_VERSION kubeadm-$KUBERNETES_VERSION kubectl-$KUBERNETES_VERSION kubernetes-cni --disableexcludes=kubernetes

# Start services
systemctl enable docker
systemctl start docker
systemctl enable kubelet
systemctl start kubelet

# Set settings needed by Docker
sysctl net.bridge.bridge-nf-call-iptables=1
sysctl net.bridge.bridge-nf-call-ip6tables=1

# Initialize the node
# Initialize the node
cat >/tmp/kubeadm.yaml <<EOF
apiVersion: kubeadm.k8s.io/v1beta1
caCertPath: /etc/kubernetes/pki/ca.crt
discovery:
  bootstrapToken:
    token: $KUBEADM_TOKEN
    apiServerEndpoint: $DNS_NAME:6443
    unsafeSkipCAVerification: true
  timeout: 20m0s
kind: JoinConfiguration
nodeRegistration:
  criSocket: /var/run/dockershim.sock
  name: $FULL_HOSTNAME
  kubeletExtraArgs:
    node-labels: "${labels}"
    cloud-provider: aws
EOF

kubeadm reset --force
kubeadm join --config /tmp/kubeadm.yaml
rm /tmp/kubeadm.yaml
