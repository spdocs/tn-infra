#!/bin/bash

set -o verbose
set -o errexit
set -o pipefail

export KUBEADM_TOKEN=${kubeadm_token}
export DNS_NAME=${dns_name}
export CLUSTER_NAME=${cluster_name}
export KUBERNETES_VERSION="${kubernetes_version}"
export CLUSTER_DNS_DOMAIN="${cluster_dns_domain}"
export CLUSTER_POD_SUBNET="${cluster_pod_subnet}"
export ADDONS="${addons}"

# Set this only after setting the defaults
set -o nounset

# We needed to match the hostname expected by kubeadm an the hostname used by kubelet
FULL_HOSTNAME="$(curl -s http://169.254.169.254/latest/meta-data/hostname)"
IP_ADDRESS="$(curl -s http://169.254.169.254/latest/meta-data/local-ipv4)"

#Set Hostname for Linux Host
sudo hostnamectl set-hostname $FULL_HOSTNAME
sudo echo "HOSTNAME=$FULL_HOSTNAME" >> /etc/sysconfig/network
sudo echo "$IP_ADDRESS $FULL_HOSTNAME" >> /etc/hosts
sudo /etc/init.d/network restart

# Make DNS lowercase
DNS_NAME=$(echo "$DNS_NAME" | tr 'A-Z' 'a-z')


# Install docker
sudo amazon-linux-extras install epel docker -y
yum update -y

# Install Kubernetes components
sudo cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=0
repo_gpgcheck=0
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
exclude=kube*
EOF

# setenforce returns non zero if already SE Linux is already disabled
is_enforced=$(getenforce)
if [[ $is_enforced != "Disabled" ]]; then
  setenforce 0
fi

yum install -y ipvsadm tc
yum install -y kubelet-$KUBERNETES_VERSION kubeadm-$KUBERNETES_VERSION kubectl-$KUBERNETES_VERSION kubernetes-cni --disableexcludes=kubernetes

# Start services
systemctl enable docker
systemctl start docker
systemctl enable kubelet
systemctl start kubelet

# Set settings needed by Docker
sysctl net.bridge.bridge-nf-call-iptables=1
sysctl net.bridge.bridge-nf-call-ip6tables=1


# Initialize the master
cat >/tmp/kubeadm.yaml <<EOF
localAPIEndpoint:
  advertiseAddress: $IP_ADDRESS
  bindPort: 6443
apiVersion: kubeadm.k8s.io/v1beta1
bootstrapTokens:
- groups:
  - system:bootstrappers:kubeadm:default-node-token
  token: $KUBEADM_TOKEN
  ttl: "0"
  usages:
  - signing
  - authentication
kind: InitConfiguration
nodeRegistration:
  criSocket: /var/run/dockershim.sock
  name: $FULL_HOSTNAME
  taints:
  - effect: NoSchedule
    key: node-role.kubernetes.io/master
    value: "master"
  kubeletExtraArgs:
    cloud-provider: aws
    allow-privileged: "true"
    cgroup-driver: "cgroupfs"
---
apiVersion: kubeadm.k8s.io/v1beta1
certificatesDir: /etc/kubernetes/pki
clusterName: $CLUSTER_NAME
etcd:
  local:
    dataDir: /var/lib/etcd
imageRepository: "k8s.gcr.io"
useHyperKubeImage: false
kind: ClusterConfiguration
kubernetesVersion: v$KUBERNETES_VERSION
controllerManager:
  extraArgs:
    cloud-provider: aws
apiServer:
  certSANs:
  - $CLUSTER_DNS_DOMAIN
  - $FULL_HOSTNAME
  - $DNS_NAME
  - $IP_ADDRESS
  extraArgs:
    cloud-provider: aws
controlPlaneEndpoint: "$DNS_NAME:6443"
networking:
  dnsDomain: $CLUSTER_DNS_DOMAIN
  podSubnet: "$CLUSTER_POD_SUBNET"
  serviceSubnet: 10.96.0.0/12
---
apiVersion: kubeproxy.config.k8s.io/v1alpha1
kind: KubeProxyConfiguration
featureGates:
  SupportIPVSProxyMode: true
mode: ipvs
ipvs:
  scheduler: sed
EOF
kubeadm reset --force
kubeadm init --config /tmp/kubeadm.yaml --ignore-preflight-errors=NumCPU
#rm /tmp/kubeadm.yaml

# Use the local kubectl config for further kubectl operations
export KUBECONFIG=/etc/kubernetes/admin.conf

# Install calico
kubectl apply -f https://docs.projectcalico.org/v3.4/getting-started/kubernetes/installation/hosted/kubernetes-datastore/calico-networking/1.7/calico.yaml


# Allow the user to administer the cluster
kubectl create clusterrolebinding admin-cluster-binding --clusterrole=cluster-admin --user=admin

# Prepare the kubectl config file for download to client (IP address)
export KUBECONFIG_OUTPUT=/home/ec2-user/kubeconfig_ip
kubeadm alpha kubeconfig user \
  --client-name admin \
  --apiserver-advertise-address $IP_ADDRESS \
  > $KUBECONFIG_OUTPUT
chown ec2-user:ec2-user $KUBECONFIG_OUTPUT
chmod 0600 $KUBECONFIG_OUTPUT

# Load addons
for ADDON in $ADDONS
do
  curl $ADDON | envsubst > /tmp/addon.yaml
  kubectl apply -f /tmp/addon.yaml
  rm /tmp/addon.yaml
done

