variable "global_tags" {
  type = "map"

  default = {
    environment                     = "demo"
    role                            = "infrastructure"
    version                         = "0.1"
    owner                           = "Sergiu Plotnicu"
    confidentiality                 = "open"
    compliance                      = "N/A"
    encryption                      = "disabled"
    KubernetesCluster               = "tn"
    "kubernetes.io/cluster/tn"      = "owned"
  }
}

variable "master_mode" {
  description = "Master Control plane mode - `ha` or `standalone`"
  default     = "standalone"
}

variable "master_instance_type" {
  description = "Master Instance type. Default wiil be `t3.small`"
  default     = "t2.micro"
}

variable "node_ondemand_count" {
  description = "Number of Ondemand Nodes for Kubernetes Cluster"
  default     = "1"
}

variable "node_ondemand_instance_type" {
  description = "Ondemand Node Instance type. Default wiil be `t3.small`"
  default     = "t2.micro"
}

variable "node_spot_count" {
  description = "Number of Spot Nodes for Kubernetes Cluster"
  default     = "0"
}

variable "node_spot_instance_type" {
  description = "Spot Node Instance type. Default wiil be `t3.small`"
  default     = "t2.micro"
}

variable "name" {
  description = "name to append to all related resources"
  default     = "tn"
}

variable "addons" {
  description = "Addons installed on kubernetes master"

  default = [
    "https://raw.githubusercontent.com/kubernetes/kubernetes/master/cluster/addons/storage-class/aws/default.yaml",
    "https://raw.githubusercontent.com/kubernetes/dashboard/v1.10.0/src/deploy/recommended/kubernetes-dashboard.yaml",
  ]
}

variable "kubernetes_cluster_public_dns_name" {
  description = "Kubernetes Public DNS Name"
  default     = ""
}

variable "kubernetes_cluster_name" {
  description = "Kubernetes Cluster Name"
  default     = "tn"
}

variable "kubernetes_cluster_version" {
  description = "Kubernetes Cluster Version"
  default     = "1.13.1"
}

variable "kubernetes_cluster_locl_dns_name" {
  description = "Kubernetes Cluster local dns name"
  default     = "tn.local"
}

variable "kubernetes_cluster_pod_subnet" {
  description = "Kubernetes Cluster Calico Subnet"
  default     = "192.168.0.0/16"
}

variable "public_dns_zone_name" {
  description = "Public DNS Name"
  default     = ""
}

variable "private_dns_zone_name" {
  description = "Private DNS Name"
  default     = ""
}

variable "keypair_name" {
  description = "KeyPair name - existing one"
  default     = "training-keypair-ireland"
}

## DB Variables

variable "db_name" {
  description = "MySQL DB Name to be created"
  default = "tnapp"
}

variable "db_username" {
  description = "MySQL Username for the DB"
  default = "tnuser"
}

variable "db_password" {
  description = "MySQL Passowrd for the DB"
  default = "tnpass123"
}
