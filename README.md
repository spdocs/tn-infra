# Terraform files to create kubernetes cluster

## Infrastructure Design

![alt text](https://bitbucket.org/spdocs/tn-infra/raw/6004ef8ed2f45aa5ce87efed6f040249ebf53d5b/tn.jpg)

## Requirements

There are some requirements before running the scripts:

* New AWS Account within free tier
* Terraform commands require proper IAM Users/Rolle assigned to local host. Next Use case assumes that local box has an IAM role attached to Instance.
* Next utilities to be installed: 
  * CURL 
  * mysql-client
  * terraform version 0.11.10 or newer is required


## Usage 

Before appling terraform scripts, check `variable.tf`. When applying terraform, it will ask for some inputs - put them in dedicated `.tfvars` or work with variables input.

### Step 1 - Terraform

```hcl
terraform init
terraform apply
```

Terraform will create a Kubernetes cluster with create - eligible for Free Tier:
* Two EC2 instances - t2.micro within an VPC
* Container Registry - same space is eligible for free tier
* RD Instance - mysql
* Some security Groups and IAM users as well
* CodeBuild project for CI

Terraform will output next data - used for APP configuration

* ecr_repository_url - docker repository
* master_instance_ip - connect via SSH to master
* rds_instance_endpoint - used for kubernetes app manifest


### Step 2 - Connect BitBucket with CodeBuild - only avaiable from Console

Due to lack of AWS Api support for codebuild and Bothbucket integration this step should be done from AWS Console

### Step 3 - Create User table - manual step - to be automated

* DB table creation, from master (using connection details):
  * yum install mysql -y
  * mysql --host= --user= --password= --database=
  * run the script to create the USER Table

### Step 4 - apply APP manifest

* APP deployment after Kubernetes is up from the Master:
  * SSH on master
  * run command `export KUBECONFIG=/etc/kubernetes/admin.conf`
  * wget https://bitbucket.org/spdocs/tn-infra/raw/75d11308c89d346ad345c2cdb2293ee676d16bef/manifests/app.yaml
  * fill with proper values for Container image and ENV variables 
  * run command `kubectl apply -f app.yaml`

## Pros and Cons

### Pros
* Infra deployed via script. quite reusable
* use of Free tier eligile service

### Cons
* only halth of the month free EC2 - 760 hours
* Kubernetes master not in HA also harder to manage - operation costs higer
* Clasic Load Balancer used - lower performance
* Stil manual steps involved

## Improvements
* Finish the CI/CD pipeline by adding some testing and additional kubernetes integration with codeBuild
* HA for Master
* Create user table of RDS deployment
* move to ALB and work with kubernetes ingress controller 
* apply IAM integration for Containers
* work more on Security Groups rules used on Kubernetes deployment
* add some ASG for Nodes
* Move GIT repo to eithe github or codecommit - for better integration with CodePipeline. use CodePipeline for CI
* SSL integration

## License

Licensed under Sergiu Plotnicu, please contact him at - sergiu.plotnicu@gmail.com


