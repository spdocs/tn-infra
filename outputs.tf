output "rds_instance_endpoint" {
  description = "DB intance endpoint to be used in the APP"
  value = "${aws_db_instance.main.endpoint}"
}

output "master_instance_ip" {
  description = "DB intance endpoint to be used in the APP"
  value = "${module.master.public_ip}"
}

output "ecr_repository_url" {
  description = "ECR container repo uri"
  value = "${aws_ecr_repository.main.repository_url}"
}
